#!/usr/bin/env python3

import netifaces as nic

def main():
    def getmac(inf):
        return nic.ifaddresses(inf)[nic.AF_LINK][0]['addr']

    def getip(inf):
        return nic.ifaddresses(inf)[nic.AF_INET][0]['addr']

    for i in nic.interfaces():
        print('\n*****************Details of Interface - ' + i + ' *****************')
        try:
            print(f"{i} MAC: {getmac(i)}")
            print(f"{i} IP: {getip(i)}")
        except:
            print('Could not get NIC info')


if __name__ == "__main__":
    main()
