#!/usr/bin/env python3

import sqlite3

conn = sqlite3.connect('test.db')
print("Opened database successfully")

cursor = conn.execute("SELECT id, name, address, salary from COMPANY")
for row in cursor:
    new_sal = 100000.00 + row[3]
    DB_UPDATE = f"UPDATE COMPANY set SALARY = {new_sal} where ID = {row[0]}"
    conn.execute(DB_UPDATE)
    conn.commit()
    print("ID = ", row[0])
    print("NAME = ", row[1])
    print("ADDRESS = ", row[2])
    print("SALARY = ", row[3], "\n")

print("Operation done successfully")
conn.close()

